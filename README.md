# Nayad DO

Dissolved Oxygen library for Nayad Modular

## Usage
### Library Installation

- Download the ZIP file from web site https://gitlab.com/hacking-ecology/nayad_do
>	nayad_do-main.zip 

- Open the Arduino IDE and add this library through menu **Sketch** -> **Include Library** -> **Add .ZIP Library** and select the zip file : `nayad_do-main.zip`
