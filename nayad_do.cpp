/*
 *            GNU General Public License version 3.
 * See <https://www.gnu.org/licenses/gpl-3.0.en.html> for details.
 * All above must be included in any redistribution
 * Copyright: 2021 Hacking Ecology
 * Editor : smsj
 * nayad_do.cpp
 * version  V0.1
 * date  2021-06
 *
 * This library is developed and tested using Nayad Modular System
 */

#include "Arduino.h"
#include "nayad_do.h"
#include "EEPROM.h"
#include <pgmspace.h>

#define VREF 3300
#define ADC_RES 4096.0
#define CAL_V 1400 //mv
#define CAL_T 25.0   //℃

float SatDoTemperature = 25.0;

const float SatValueTab[41] PROGMEM = {      //Set the saturation of DO concentrations at various temperatures
  14.46, 14.22, 13.82, 13.44, 13.09,
  12.74, 12.42, 12.11, 11.81, 11.53,
  11.26, 11.01, 10.77, 10.53, 10.30,
  10.08, 9.86,  9.66,  9.46,  9.27,
  9.08,  8.90,  8.73,  8.57,  8.41,
  8.25,  8.11,  7.96,  7.82,  7.69,
  7.56,  7.43,  7.30,  7.18,  7.07,
  6.95,  6.84,  6.73,  6.63,  6.53,
  6.41,
};

 
Nayad_DO::Nayad_DO(uint8_t pin){
	this->pin = pin;
    this->EEPROM_offset = (pin) * EEPROM_SIZE_CONST;
    //Map the calibration parameters locations to the analog pin numbers assure they will lay them out in EEPROM
    //We definea an EEPROM_SIZE_CONST for every struct we record
}

bool Nayad_DO::begin(){
    EEPROM.begin(4096);
	if((EEPROM.read(this->EEPROM_offset) == magic_char)
    && (EEPROM.read(this->EEPROM_offset + sizeof(uint8_t)) == NAYAD_DO)){
		EEPROM.get(this->EEPROM_offset,DO);
		return true;
    }
	return false;
}

float Nayad_DO::read_voltage() {
    float voltage_mV = 0;
    for (int i = 0; i < volt_avg_len; ++i) {
      voltage_mV += analogRead(this->pin) / ADC_RES * VREF;
    }
    voltage_mV /= volt_avg_len;
    return voltage_mV;
}

float Nayad_DO::read_do_percentage(float voltage_mV) {
    return voltage_mV * 100.0 / this->DO.full_sat_voltage;
}

float Nayad_DO::read_do_concentration(float voltage_mV, float temperature_c) {
    //this->_doValue = pgm_read_float_near(&SatValueTab[0] + (int)(this->SatDoTemperature + 0.5)) * this->voltage_mV / this->DEFAULT_SAT_VOLTAGE;
    //float V_saturation = (float)CAL_V + (float)35 * temperature_c - (float)CAL_T * 35;
    //return (voltage_mV * SatValueTab[temperature_c] / V_saturation);
    float V_saturation = (float)CAL_V + (float)35.0 * temperature_c - (float)CAL_T * (float)35.0;
    return (voltage_mV * SatValueTab[0] / V_saturation);
}

float Nayad_DO::cal() {
    this->DO.full_sat_voltage = read_voltage();
    EEPROM.put(this->EEPROM_offset,DO);
    EEPROM.commit(); 

}

float Nayad_DO::cal_clear() {
    this->DO.full_sat_voltage = DEFAULT_SAT_VOLTAGE;
    EEPROM.put(this->EEPROM_offset,DO);
    EEPROM.commit();
}

float Nayad_DO::read_do_percentage() {
    return(read_do_percentage(read_voltage()));
}
