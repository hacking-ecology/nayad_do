/***********************************************************************
  # This code is developed to be used with Nayad Modular DO version
  # Editor : smsj
  # Version: 1.1
  # Date   : 2021.06.30
  # Sensor: Dissolved Oxygen

            GNU General Public License version 3.
  See <https://www.gnu.org/licenses/gpl-3.0.en.html> for details.
  All above must be included in any redistribution
************************************************************************/

////////////////////////////////////////////////////////////////////////
////////////////////// Calibration directions /////////////////////////
///////////////////////////////////////////////////////////////////////
/************************************************************************
  At the serial screen:

  CAL 

  Start calibration process (use 100% saturated buffer)

  Directions:
  1 - There are two methods to calibrate the sensor:

  * Expose the wet probe to the air.
  * Immerse the probe in saturated dissolved oxygen water.

    Only calibrate the saturated dissolved oxygen at a stable temperature.

  - Expose the wet probe to the air
    1 - Prepare the probe
    2 - Wet the probe in pure water and remove the excess of water moving gently the probes.
    3 - Expose the probe to the air and maintain proper air flow (do not use a fan to blow).
    4 - After the output voltage is stable, record the voltage, which is the saturated dissolved oxygen voltage at the current temperature

  - Immerse the probe in saturated dissolved oxygen water
    1 - Prepare a cup of purified water and use one of the following methods to make saturated oxygen water.
    2 - Use an air pump to continuously inflate the water for 10 minutes to saturate the dissolved oxygen
    3 - Stop pumping, and put the probe after the bubbles disappear
    4 - After placing the probe, keep stirring slowly while avoiding any bubbles.
    5 - After the output voltage stable, record the temperature and voltage
  
************************************************************************/

#include "nayad_do.h"

Nayad_DO DO = Nayad_DO(33);

uint8_t user_bytes_received = 0;
const uint8_t bufferlen = 32;
char user_data[bufferlen];


void parse_cmd(char* string) {
  strupr(string);
  String cmd = String(string);
  if(cmd.startsWith("CAL")){
    int index = cmd.indexOf(',');
    if(index != -1){
      String param = cmd.substring(index+1, cmd.length());
      if(param.equals("CAL CLEAR")){
        DO.cal_clear();
        Serial.println("CALIBRATION CLEARED");
      }
    }
    else{
      DO.cal();
      Serial.println("DO PROBE CALIBRATED");
    }
  }
}

void setup() {
  Serial.begin(115200);
  delay(200);
  Serial.println(F("Use command \"CAL\" to calibrate the circuit to 100% saturation in air\n\"CAL,CLEAR\" clears the calibration"));
  if(DO.begin()){
    Serial.println("Loaded EEPROM");
  }
}

void loop() {
  if (Serial.available() > 0) {
    user_bytes_received = Serial.readBytesUntil(13, user_data, sizeof(user_data));
  }

  if (user_bytes_received) {
    parse_cmd(user_data);
    user_bytes_received = 0;
    memset(user_data, 0, sizeof(user_data));
  }
  
  Serial.println(DO.read_do_percentage());
  Serial.println(DO.read_voltage());
  Serial.println(DO.read_do_concentration());  
  
  delay(1000);
}
