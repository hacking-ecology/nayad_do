/*
 *            GNU General Public License version 3.
 * See <https://www.gnu.org/licenses/gpl-3.0.en.html> for details.
 * All above must be included in any redistribution
 * Copyright: 2021 Hacking Ecology
 * Editor : smsj
 * nayad_do.h
 * version  V0.1
 * date  2021-06
 *
 * This library is developed and tested using Nayad Modular System
 */


#ifndef DO_NAYAD_H
#define DO_NAYAD_H

#include "Arduino.h"

//#define DEFAULT_SAT_VOLTAGE_CONST (40.0*11.0)
#define DEFAULT_SAT_VOLTAGE_CONST 1440.15 //1127.6

class Nayad_DO {
	public:
		Nayad_DO(uint8_t pin);

        enum nayad_type{
            NAYAD_DO,
        };
		
		bool begin();
	
		virtual float read_voltage();
		float read_do_percentage(float voltage_mV);
		float read_do_percentage();
        float read_do_concentration(float voltage_mV, float temperature_c);
        //float read_do_concentration();
        float cal();
        float cal_clear();
        
    protected:
		uint8_t pin = 33;
		static const int volt_avg_len = 1000;
        static const uint8_t EEPROM_SIZE_CONST = 16;
		static const uint8_t magic_char = 0xAA;
        
        int16_t EEPROM_offset = 0;
	private:
		
        const float DEFAULT_SAT_VOLTAGE = DEFAULT_SAT_VOLTAGE_CONST;
		struct DO {
		  const uint8_t magic = magic_char;
          const enum nayad_type type = NAYAD_DO;
		  float full_sat_voltage = DEFAULT_SAT_VOLTAGE_CONST;
		};
		struct DO DO;
};

#endif
